#!/bin/bash
#
# one-off preparation
#
source env.sh

sudo apt update
sudo apt install build-essential git-buildpackage

./prepare-01-apt-source.sh
./prepare-02-salsa.sh
./prepare-03-upstream.sh

