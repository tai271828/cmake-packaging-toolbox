#!/bin/bash
#
# one-off preparation to the packaging source
#
source env.sh

WORK_DIR=${WORK_DIR_SALSA}-pre-source

rm -rf ${WORK_DIR}


mkdir -p ${WORK_DIR}
pushd ${WORK_DIR}

git clone https://salsa.debian.org/cmake-team/cmake.git -b debian/3.18.4-2

cd cmake
git checkout -b master
# pristine-tar data for cmake_3.18.4.orig.tar.gz
git checkout -b pristine-tar 9f8a3c1ce92d0424cba061ece908eae28093752b
git checkout -b upstream upstream/3.18.4

git checkout master
gbp import-orig --verbose --pristine-tar --uscan --upstream-version=3.20.2

# --> Packaging by modifying master branch
#
# --> Once completing, release by:
#gbp dch --new-version=3.20.2-0ubuntu1~20.04.1~soss.1 --distribution=focal
#git commit -a -m "Release to focal"
#
# --> Build the package
# --> Upload

popd

