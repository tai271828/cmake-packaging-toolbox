#!/bin/bash
#
# one-off preparation to download cmake source from ubuntu archive
#
source env.sh

rm -rf ${WORK_DIR_APT_SOURCE}

mkdir -p ${WORK_DIR_APT_SOURCE}
pushd ${WORK_DIR_APT_SOURCE}

sudo sed -i '/^#\sdeb-src /s/^#\s//' "/etc/apt/sources.list"
sudo apt update
sudo apt install build-essential -y
sudo apt build-dep cmake -y

# no need to use sudo to fetch source
# it may cause issue about lack of permission when dpkg-* applied to the source
# files
apt-get source cmake

#cd cmake-3.16.3
#time dpkg-buildpackage -us -uc -ui -i -I

popd

