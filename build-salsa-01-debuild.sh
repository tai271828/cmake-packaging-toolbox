#!/bin/bash
source env.sh

pushd ${WORK_DIR_SALSA}/cmake-soss
git restore ./
git clean -df
rm -rf ./Build

# dsc and debian.tar.xz will be generated automatically by dpkg-buildpackage
gbp export-orig --verbose

git restore ./
git clean -df
rm -rf ./Build

#dpkg-source --before-build .
#debian/rules clean
#dpkg-source -b .
#debian/rules binary
# will still use multiple cores so should be fast. dgx-1 will take around 11 min
#time dpkg-buildpackage -us -uc > /tmp/build-salsa-01-dpkg-buildpackage-us-uc.log 2>&1
# gbp seems to call debuild and debuild seems to call dpkg-buildpackage
time debuild -us -uc > /tmp/build-salsa-01-debuild-us-uc.log 2>&1

# - if we use dpkg-buildpackage then we do not need to worry about providing key at this moment
# - if gbp buildpackage could build succesfully, then dpkg-buildpackage should build succesfully
# because gbp buildpackage calls dpkg-buildpackage
# - gbp buildpackage will generate orig so we do not need gbp export-orig above
# - will use multiple cores so should be fast. dgx-1 will take around 11 min
#time gbp buildpackage --git-ignore-new > /tmp/build-salsa-gbp.log 2>&1

popd
