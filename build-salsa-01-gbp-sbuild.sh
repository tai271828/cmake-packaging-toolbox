#!/bin/bash
source env.sh

pushd ${WORK_DIR_SALSA}/cmake-soss/cmake-soss
git restore ./
git clean -df
rm -rf ./Build

git checkout ${GIT_BUILD_BRANCH}
git pull

# - if we use dpkg-buildpackage then we do not need to worry about providing key at this moment
# - if gbp buildpackage could build succesfully, then dpkg-buildpackage should build succesfully
# because gbp buildpackage calls dpkg-buildpackage
# - gbp buildpackage will generate orig so we do not need gbp export-orig above
# - will use multiple cores so should be fast. dgx-1 will take around 11 min
#
# if there is debian/version tag, then gbp will try to sign it
time gbp buildpackage --git-debian-branch=${GIT_BUILD_BRANCH} --git-builder="sbuild --resolve-alternatives"

popd
