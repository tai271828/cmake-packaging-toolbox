#!/bin/bash
#
# one-off preparation to download cmake source from the upstream
#
source env.sh

rm -rf ${WORK_DIR_UPSTREAM}

mkdir -p ${WORK_DIR_UPSTREAM}
pushd ${WORK_DIR_UPSTREAM}

git clone ${UPSTREAM_REPO} -b ${UPSTREAM_TAG} --single-branch
cd cmake
git checkout -b upstream-${UPSTREAM_TAG}

popd

