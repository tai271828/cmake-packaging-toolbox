#!/bin/bash
#
# one-off preparation to download cmake source with debian directory from salsa
#
source env.sh

rm -rf ${WORK_DIR_SALSA}


mkdir -p ${WORK_DIR_SALSA}
pushd ${WORK_DIR_SALSA}

git clone ${SALSA_REPO} -b ${SALSA_BRANCH}

popd

